# Cross compilation tools for gstreamer

**Rationale:** Cross compiling gstreamer can be tricky. In certain cases compilers/linkers are not clever enough to pick right header files and/or libraries even though they are given the `--sysroot` option, and they pick those files from the PC host rootfs instead. While investigating the ultimate root cause of this situation is an interesting excercise, a viable workaround is by using the same distribution on a PC and in the sysroot/target rootfs. A systemized way to achieve that is by running the cross toolchain in a Docker container, so that all the developers use the host rootfs which matches the target sysroot even if they use various Linux distributions on their hosts.

**General idea:** One of our preferred ways of running development boards is by having rootfs on an NFS-exported volume, so to combine the two the following approach has been taken: The cross toolchain lives in a Docker container, while the cross compilation sysroot is a directory external to the container but mounted as a Docker volume. This same directory is served by the NFS server. Both the Docker image and the sysroot/rootfs are built using the tools from this repository. It is the contents of this repository that is responsible for making sure the image and the sysroot/rootfs are consistent and contain enough stuff to serve their purpose well.

## What is what of what

* Dockerfile

Contains instructions for Docker to build the host image. Please note that while using he Dockerfile directly is a perfectly valid approach, you can also pull ready-made images from `registry.gitlab.collabora.com/collabora/gst-build-sdk/gst-build-sdk:main`. More on that further into this document. The image is an x86_64 image that runs on your PC host and contains the (sanitized) cross toolchain.

* rootfs.yaml

Contains instructions for [debos](https://github.com/go-debos/debos) to prepare the target (arm64) sysroot/rootfs, which contains both build-time gstreamer dependencies and some essential tools needed to use the generated filesystem as rootfs on your development board. The result is a `.tar.gz` archive, which needs to be uncompressed and then passed to Docker as a volume. More on obtaining and preparing the rootfs later.

* docker.sh

Is a driver file for running Docker in two contexts: for generating a Docker image and for running the image. It is this file that passes a path to the sysroot/rootfs to `docker` invocation. You only need this file, all the rest can be downloaded without running Docker or debos (but of course you can use Dockerfile and rootfs.yaml locally if you want).

* gst-build-sdk-rootfs-arm64.tar.gz

Not a part of this repo, but a product of running debos with `rootfs.yaml`. You can run debos on your own, but you can also download it from build artifacts in our gitlab's CI. More on that later.

## Obtaining docker images from our registry

To download the host docker image:

`docker pull registry.gitlab.collabora.com/collabora/gst-build-sdk/gst-build-sdk:main`

Please note that Docker places the received images in its configured location, e.g. `/var/lib/docker`, so `docker.sh` above will *know* how to access them. Teaching you Docker is beyond the scope of this document, you can go [there](https://docs.docker.com/).

## Obtaining the sysroot/rootfs

Our gitlab provides [a list](https://gitlab.collabora.com/collabora/gst-build-sdk/-/jobs) of CI jobs resulting from commits to this repo. Next to each item in the list there's a "Download artifacts" button which you can use (they come packed in artifacts.zip even if there's just one artifact), alternatively you can go to job details and "Browse" the artifacts there to select just the `.tar.gz`. Either way, you end up having `gst-build-sdk-rootfs-arm64.tar.gz`.

This file contains full rootfs for your target board, which is equipped with enough packages to serve as cross-compilation sysroot at compile time. Since it is a complete rootfs, you need to unpack it as root, otherwise tar won't be able to re-create certain files. The extracted directory path is passed in the `SYSROOT` environment variable to `docker.sh`.

In case you forgot, this is an example invocation which extracts the .tar.gz to a specified directory:

`sudo tar -zxf gst-build-sdk-rootfs-arm64.tar.gz --directory /home/user/your/rootfs/arm64/`

That same extracted directory can then be exported as an NFS volume. Explaining how to do it is beyond the scope of this document; googling around will return many instructions on that.

## Running the builder

The intended usage of the Docker image is mainly to invoke the cross compilation toolchain to generate correct binaries. You run the builder like this:

`SYSROOT=/home/user/your/rootfs/arm64 docker.sh run`.

Thanks to the Docker magic the directory with your sysroot/rootfs is accessible from inside the container, under exactly the same path as in your host system, and when you run the container you are moved to that directory (no effect on your current directory in the host).

The container and the sysroot/rootfs contain enough stuff to successfuly cross compile gstreamer, and a usual procedure is in order. Below is a short summary. We assume you have already checked out gst-build - that step can be done outside the container, in the `/home/user` directory of the sysroot/rootfs. The container provides `git` as well.

`git clone https://gitlab.freedesktop.org/gstreamer/gst-build.git`

Then configure the build with `meson`, to cross compile a meson `cross-file` is needed. An example cross file:

```
[host_machine]
system = 'linux'
cpu_family = 'aarch64'
cpu = 'aarch64'
endian = 'little'

[properties]
sys_root = '/home/user/your/rootfs/arm64'
pkg_config_libdir = '/home/user/your/rootfs/arm64/usr/lib/pkgconfig:/home/user/your/rootfs/arm64/usr/lib/aarch64-linux-gnu/pkgconfig:/home/user/your/rootfs/arm64/usr/share/pkgconfig'

c_args = ['--sysroot=/home/user/your/rootfs/arm64']
cpp_args = ['--sysroot=/home/user/your/rootfs/arm64', '-I/home/user/your/rootfs/arm64/usr/include/c++/10']
c_link_args = ['--sysroot=/home/user/your/rootfs/arm64']
cpp_link_args = ['--sysroot=/home/user/your/rootfs/arm64']

[binaries]
c = 'aarch64-linux-gnu-gcc'
cpp = 'aarch64-linux-gnu-g++'
ar = 'aarch64-linux-gnu-ar'
strip = 'aarch64-linux-gnu-strip'
pkgconfig = 'pkg-config'
```

you obviously want to adapt it to your needs (mainly by changing all occurences of the sys_root variable's value). Then e.g.:

`meson -Dvaapi=disabled -Dlibav=disabled -Drtsp_server=disabled -Dgst-examples=disabled -Dgst-plugins-good:v4l2-libv4l2=enabled --cross-file ../meson-arm64.txt build`

followed by

`ninja -C build`.

## Runnig the cross compiled gstreamer

For that you need to boot your development board, with your sysroot/rootfs exported via NFS used as rootfs. Explaining how to boot with NFS rootfs is beyond the scope of this document, but you can look [there](https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt).

There's a `user` user in the rootfs, with the same password. The user is capable of invoking `sudo`.

For developers gst-build provides an `uninstalled` environment, which is a way of setting environment variables in such a way, that all the gstreamer stuff is picked from your build directory rather than from system location even if you have distribution gstreamer installed. If the build has been a cross build on a different machine (and that is exactly the case), you need to specify the `--sysroot` argument, which is used to adjust the paths accordingly, so that your gstreamer build cross-compiled on a PC works in the native system. To enter that environment you invoke:

`./gst-env.py --sysroot /home/user/your/rootfs/arm64`

Please note that the `--sysroot` path is as it was in the PC host at compile time. That's not a mistake, even though such a path does not exist in the system on the development board. I'm only guessing, but it seems to me that the purpose of this argument is exactly to *remove* the build-time-specific prefix from certain paths.

Once in the environment:

```
[gst-master] user@gst-build-sdk:~/gst-build$ which gst-inspect-1.0
/home/user/gst-build/build/subprojects/gstreamer/tools/gst-inspect-1.0

[gst-master] user@gst-build-sdk:~/gst-build$ gst-inspect-1.0 video4linux2
Plugin Details:
  Name                     video4linux2
  Description              elements for Video 4 Linux
  Filename                 /home/user/gst-build/build/subprojects/gst-plugins-good/sys/v4l2/libgstvid
eo4linux2.so
  Version                  1.19.0.1
  License                  LGPL
  Source module            gst-plugins-good
  Binary package           GStreamer Good Plug-ins git
  Origin URL               Unknown package origin

  v4l2src: Video (video4linux2) Source
  v4l2sink: Video (video4linux2) Sink
  v4l2radio: Radio (video4linux2) Tuner
  v4l2deviceprovider: Video (video4linux2) Device Provider
  v4l2h264enc: V4L2 H.264 Encoder

  5 features:
  +-- 4 elements
  +-- 1 device providers
```
